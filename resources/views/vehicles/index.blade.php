@extends('layouts.app')

@section('title','Vehicle')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"></a>Vehicles</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">   
                <form method="get" id="search-form">
                    <div class="col-md-1 text-center">
                      <h4>Search</h4>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group no-border">
                            <input type="text" class="form-control column-filter" name="filter[name]" placeholder="Cari berdasarkan Nama" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}">
                            <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group no-border">
                            <input type="text" class="form-control column-filter" name="sort[identifier]" placeholder="Cari berdasarkan Identifier" value="{{ !empty($sort['identifier']) ? $sort['identifier'] : '' }}">
                            <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary btn-block pull-right"><i class="fa fa-search">Cari</i> </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">List Vehicles</h3>
          <div class="box-tools pull-right">
            <a href="{{ route('vehicles.create') }}" class="btn btn-sm btn-primary"> Add New Vehicle</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Identifier</th>
                <th class="text-center">Brand</th>
                <th class="text-center">Name</th>
                <th class="text-center">Color</th>
                <th class="text-center">Type</th>
                <th class="text-center">Year</th>
                <th class="text-center">Odometer</th>
                <th class="text-center" colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                @if($vehicles->count() > 0)
                  @foreach($vehicles as $vehicle)
                  <td class="text-center">{{ $loop->iteration }}</td>
                  <td class="text-center">{{ $vehicle->identifier }}</td>
                  <td class="text-center">{{ $vehicle->brand->name }}</td>                      
                  <td class="text-center">{{ $vehicle->name }}</td>
                  <td class="text-center">{{ $vehicle->color }}</td>
                  <td class="text-center">{{ $vehicle->type }}</td>
                  <td class="text-center">{{ $vehicle->year }}</td>
                  <td class="text-center">{{ $vehicle->odometer }}</td>
                  <td class="text-center">
                  <a href="{{route('vehicles.show', $vehicle->id)}}" class="btn btn-warning btn-sm">Details</a>
                  <!-- <form action="{{route('vehicles.show', $vehicle->id)}}" method="get"> -->
                    
                    <!-- <a href="{{route('vehicles.show', $vehicle->id)}}">Details</a> -->
                  <!-- </form> -->
                  </td>
              </tr>
                  @endforeach                
                @else
                  <tr>
                      <td colspan="8" class="text-center">
                      No Data
                      </td>
                  </tr>              
                @endif
            </tbody>
          </table>
          @php
            $filters = [];
              foreach($filter as $key => $value)
              {
                $filters['filter['.$key.']'] =$value; 
              }
          @endphp
            {{ $vehicles->appends($filters)->links() }}
        </div>
        <!-- /.box-body -->
      </div>
    </div>  
  </div>  
@endsection
