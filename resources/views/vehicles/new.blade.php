@extends('layouts.app')

@section('title','Vehicle')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Simple</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Vehicle</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['vehicles.store'], 'method' => 'POST', 'files'=> true]) !!}
        {{ Form::hidden('user_id', Auth::User()->id) }}
          <div class="form-group">
            {{ Form::label("Identifier") }}
            {{ Form::text("identifier", null, [ 'id' => 'identifier', 'class' => 'form-control', 'placeholder' => 'identifier']) }}
            @if ($errors->has('identifier'))
                <div class="error">{{ $errors->first('identifier') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Brand") }}
          {{ Form::select("brand_id", $brands, null , ["class" => "form-control"]) }}
            @if ($errors->has('brand_id'))
                <div class="error">{{ $errors->first('brand_id') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Name") }}
            {{ Form::text("name", null, [ 'id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name']) }}
            @if ($errors->has('name'))
                <div class="error">{{ $errors->first('name') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Color") }}
            {{ Form::text("color", null, [ 'id' => 'color', 'class' => 'form-control', 'placeholder' => 'Color']) }}
            @if ($errors->has('color'))
                <div class="error">{{ $errors->first('color') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Type") }}
            {{ Form::text("type", null, [ 'id' => 'type', 'class' => 'form-control', 'placeholder' => 'Type']) }}
            @if ($errors->has('type'))
                <div class="error">{{ $errors->first('type') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Year") }}
            {{ Form::text("year", null, [ 'id' => 'year', 'class' => 'form-control', 'placeholder' => 'Odometer']) }}
            @if ($errors->has('year'))
                <div class="error">{{ $errors->first('year') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Odometer") }}
            {{ Form::text("odometer", null, [ 'id' => 'odometer', 'class' => 'form-control', 'placeholder' => 'Odometer']) }}
            @if ($errors->has('odometer'))
                <div class="error">{{ $errors->first('odometer') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Status :") }}<br>
            {{Form::radio("status", '1' , true) }}Active <br>
            {{Form::radio("status", '0',) }}Non Active
            @if ($errors->has('status'))
                <div class="error">{{ $errors->first('status') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Picture") }}
            {{ Form::file("picture",["class" => "form-control",]) }}
            @if ($errors->has('picture'))
                <div class="error">{{ $errors->first('picture') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
          






          <!-- <div class="form-group">
          {{ Form::label("Brand") }}
          <select name="brand_id" id="">
            <option value="" selected>Choose Brand</option>
            @foreach($brands as $key => $value)
              <option value="{{ $key }}" {{ 1 == $key ? 'selected' : null }}>{{ $value }}</option>
            @endforeach            
          </select>
            @error('brand_id')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div> -->
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
