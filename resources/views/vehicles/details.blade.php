@extends('layouts.app')

@section('title')
  <h3>
    Details ( {{ $vehicle->name }} : {{$vehicle->identifier}})
  </h3>
@endsection

@section('breadcrumb')  
    <li><a href="{{route('vehicles.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Details</li>
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-6">
    </div>
    <div class="col-md-3">
    </div>
    <div class="col-md-3">
      <form method="POST" action="{{ route('vehicles.destroy', $vehicle->id) }}">
      <input type="hidden" name="_method" value="DELETE">
      @csrf
      <a href="{{ route('vehicles.edit', $vehicle->id) }}" class="btn btn-warning btn-sm">Edit</a>
        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')">Delete</button>  
      </form>
    </div>
  </div>
</div>
<br>
<div class="container">
  <div class="row">
    <!-- head -->
    <div class="card">
      <div class="col-md-3">  
        <div class="card-body">
          <blockquote class="blockquote mb-0">
            <h4>Plat Nomer</h4>
            <h4>Brand</h4>
            <h4>Name</h4>
            <h4>Color</h4>
            <h4>Year</h4>
            <h4>Type</h4>
            <h4>Odometer</h4>
          </blockquote>
        </div>
      </div>
      <div class="col-md-3">  
        <div class="card-body">
            <h4>{{ $vehicle->identifier }}</h4>
            <h4>{{ $vehicle->brand->name }}</h4>
            <h4>{{ $vehicle->name }}</h4>
            <h4>{{ $vehicle->color }}</h4>
            <h4>{{ $vehicle->year }}</h4>
            <h4>{{ $vehicle->type }}</h4>
            <h4>{{ $vehicle->odometer }}</h4>
        </div>
      </div>
      <div class="col-md-3">  
        <div class="card-body">
          <img src="{{ Storage::disk('images')->url($vehicle->picture) }}" style="width:100%" alt="" srcset="">
        </div>
      </div>
    </div>
  </div>
</div>

<br><br>

<div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">History</h3>
          <div class="box-tools pull-right">
            <a href="{{ route('vehicles.services.create', $vehicle->id) }}" class="btn btn-sm btn-primary"> Add services</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="" class="text-center">No</th>
                <th width="" class="text-center">Date</th>
                <th width="" class="text-center">Odometer</th>
                <th width="" class="text-center">Description</th>
                <th width="" class="text-center" colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @if($inspect->isEmpty())
                  <tr>
                    <td colspan="5" class="text-center">
                      No Data
                    </td>
                  </tr>
                @else
                  @foreach($inspect as $inspect)
                    <tr>
                     <td>{{ $loop->iteration }}</td>
                     <td>{{ $inspect->date }}</td>
                     <td>{{ $inspect->odometer }}</td>
                     <td>{{ $inspect->description }}</td>
                     <td class="text-center">
                     <form method="POST" action="{{ route('vehicles.services.destroy', [$vehicle->id, $inspect->id]) }}">
                        <input type="hidden" name="_method" value="delete">
                        @csrf
                        <a href="{{ route('vehicles.services.edit', ['vehicle' => $vehicle->id, 'service' => $inspect->id]) }}" class="btn btn-warning btn-sm">Edit</a>
                          <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')">Delete</button>  
                      </form>
                     </td>
                    </tr>
                  @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>  
  </div>  

  <br>

  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Schedules</h3>
          <div class="box-tools pull-right">
            <a href="{{ route('vehicles.schedules.create', $vehicle->id) }}" class="btn btn-sm btn-primary"> Add Schedule</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="" class="text-center">No</th>
                <th width="" class="text-center">Date</th>
                <th width="" class="text-center">Title</th>
                <th width="" class="text-center">Status</th>
                <th width="" class="text-center">Odometer</th>
                <th width="" class="text-center">Description</th>
                <th width="" class="text-center" colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @if($schedule->isEmpty())
                  <tr>
                    <td colspan="5" class="text-center">
                      No Data
                    </td>
                  </tr>
                @else
                  @foreach($schedule as $schedule)
                    <tr>
                     <td>{{ $loop->iteration }}</td>
                     <td>{{ $schedule->date }}</td>
                     <td>{{ $schedule->title }}</td>
                     <td>{{ $schedule->status }}</td>
                     <td>{{ $schedule->odometer }}</td>
                     <td>{{ $schedule->description }}</td>
                     <td class="text-center">
                     <form method="POST" action="{{ route('vehicles.schedules.destroy', [$vehicle->id, $schedule->id]) }}">
                        <input type="hidden" name="_method" value="delete">
                        @csrf
                        <a href="{{ route('vehicles.schedules.edit', ['vehicle' => $vehicle->id, 'schedule' => $schedule->id]) }}" class="btn btn-warning btn-sm">Edit</a>
                          <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')">Delete</button>  
                      </form>
                     </td>
                    </tr>
                  @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>  
  </div>  

@endsection
