@extends('layouts.app')

@section('title','New Vehicle')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('vehicles.index')}}">Tables</a></li>
    <li class="active">New Service</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Service</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['vehicles.services.store', $vehicle->id], 'method' => 'POST']) !!}
        {{ Form::hidden('user_id', Auth::User()->id) }}
        {{ Form::hidden('vehicle_id', $vehicle->id) }}
          <div class="form-group">
            {{ Form::label("Date") }}
            {{ Form::date("date", null, [ 'class' => 'form-control']) }}
            @if ($errors->has('date'))
                <div class="error">{{ $errors->first('date') }}</div>
            @endif
          </div>
          <div class="form-group">
            {{ Form::label("Odometer") }}
            {{ Form::text("odometer", null, [ 'id' => 'odometer', 'class' => 'form-control', 'placeholder' => 'Odometer']) }}
            @if ($errors->has('odometer'))
                <div class="error">{{ $errors->first('odometer') }}</div>
            @endif
          </div>
          <div class="form-group">
            {{ Form::label("Description") }}
            {{ Form::textarea("description", null, [ 'id' => 'description', 'class' => 'form-control', 'placeholder' => 'Put The Description Here']) }}
            @if ($errors->has('description'))
                <div class="error">{{ $errors->first('description') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
