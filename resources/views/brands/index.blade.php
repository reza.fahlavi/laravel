@extends('layouts.app')

@section('title','Brand')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Simple</li>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">List Brand</h3>
          <div class="box-tools pull-right">
            <a href="{{ route('brands.create') }}" class="btn btn-sm btn-primary"> Add New Brand</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="" class="text-center">No</th>
                <th width="" class="text-center">Code</th>
                <th width="" class="text-center">Name</th>
                <th width="" class="text-center" colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @if($brands->isEmpty())
                  <tr>
                    <td colspan="3" class="text-center">
                      No Data
                    </td>
                  </tr>
                @else
                  @foreach($brands as $brand)
                    <tr>
                     <td>{{ $loop->iteration }}</td>
                     <td>{{ $brand->code }}</td>
                     <td>{{ $brand->name }}</td>
                     <td class="text-center">
                     <form method="POST" action="{{ route('brands.destroy', $brand->id) }}">
                      <input type="hidden" name="_method" value="delete">
                      @csrf
                      <a href="{{ route('brands.edit', $brand->id) }}" class="btn btn-warning btn-sm">Edit</a>
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')">Delete</button>  
                     </form>
                     </td>
                     
                    </tr>
                  @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>  
  </div>  
@endsection
