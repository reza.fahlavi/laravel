@extends('layouts.app')

@section('title','Brand')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('brands.index') }}">{{ $brand->name }}</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
  <div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Edit Brand</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->      
      <form action="{{ route('brands.update', $brand->id) }}" method="POST">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        <div class="box-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $brand->name }}">
            @error('name')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Code</label>
            <input type="text" class="form-control" id="code" name="code" placeholder="The Code" value="{{ $brand->code }}">
            @error('name')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <label class="btn btn-secondary {{ $brand->active == '1' ? 'active' : null }}">
                <input type="radio" name="active" id="option1" value="1" autocomplete="off"> Active
              </label>
              <label class="btn btn-secondary {{ $brand->active == '0 ' ? 'active' : null }}">
                <input type="radio" name="active" id="option2" value="0" autocomplete="off"> Non Active
              </label>
            </div>
            @error('active')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>  
@endsection   
