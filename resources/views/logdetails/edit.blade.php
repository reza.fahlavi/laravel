@extends('layouts.app')

@section('title','Maintenance Log Detail')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">New Maintenance</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Maintenance Log</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['logdetails.update', $detail->id], 'method' => 'PUT', 'files' => true]) !!}
          <div class="form-group">
          {{ Form::label("Work Item") }}
          <select name="workitem_id" class="form-control" id="">
            <option value="" selected>Choose Work Item</option>
            @foreach($work as $key => $value)
              <option value="{{ $key }}" {{ $detail->workitem_id == $key ? 'selected' : null }}>{{ $value }}</option>
            @endforeach            
          </select>
            @error('workitem_id')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
          {{ Form::label("Maintanance Log") }}
          <select name="maintenancelog_id" class="form-control" id="">
            <option value="" selected>Choose log</option>
            @foreach($log as $key => $value)
              <option value="{{ $key }}" {{ $detail->maintenance_log_id == $key ? 'selected' : null }}>{{ $value }}</option>
            @endforeach            
          </select>
            @error('maintenancelog_id')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
          {{ Form::label("Amount") }}
            {{ Form::text("amount", $detail->amount, [ 'id' => 'amount', 'class' => 'form-control', 'placeholder' => 'Amount']) }}
            @if ($errors->has('amount'))
                <div class="error">{{ $errors->first('amount') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
