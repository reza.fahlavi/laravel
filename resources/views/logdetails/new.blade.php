@extends('layouts.app')

@section('title','Maintenance Log Detail')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">New Maintenance</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Maintenance Log</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['logdetails.store'], 'method' => 'POST', 'files' => true]) !!}
          <div class="form-group">
          {{ Form::label("Work Item") }}
          {{ Form::select("workitem_id", $works, null , ["class" => "form-control"]) }}
            @if ($errors->has('workitem_id'))
                <div class="error">{{ $errors->first('workitem_id') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Maintanance Log") }}
          {{ Form::select("maintenancelog_id", $logs, null , ["class" => "form-control"]) }}
            @if ($errors->has('maintenance_log_id'))
                <div class="error">{{ $errors->first('maintenance_log_id') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Amount") }}
            {{ Form::text("amount", null, [ 'id' => 'amount', 'class' => 'form-control', 'placeholder' => 'Amount']) }}
            @if ($errors->has('amount'))
                <div class="error">{{ $errors->first('amount') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
