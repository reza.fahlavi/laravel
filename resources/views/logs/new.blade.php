@extends('layouts.app')

@section('title','Vehicle')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Simple</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Vehicle</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['logs.store'], 'method' => 'POST', 'files'=> true]) !!}
        {{ Form::hidden('user_id', Auth::User()->id) }}
          <div class="form-group">
          {{ Form::label("vehicle") }}
          {{ Form::select("vehicle_id", $vehicle, null , ["class" => "form-control"]) }}
            @if ($errors->has('vehicle_id'))
                <div class="error">{{ $errors->first('vehicle_id') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Work Shop") }}
          {{ Form::select("workshop_id", $workshop, null , ["class" => "form-control"]) }}
            @if ($errors->has('workshop_id'))
                <div class="error">{{ $errors->first('workshop_id') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Date") }}
            {{ Form::date("date", null, [ 'id' => 'date', 'class' => 'form-control', 'placeholder' => 'Date']) }}
            @if ($errors->has('date'))
                <div class="error">{{ $errors->first('date') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Odometer") }}
            {{ Form::text("odometer", null, [ 'id' => 'odometer', 'class' => 'form-control', 'placeholder' => 'Odometer']) }}
            @if ($errors->has('odometer'))
                <div class="error">{{ $errors->first('odometer') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Notes") }}
            {{ Form::textarea("notes", null, [ 'id' => 'notes', 'class' => 'form-control', 'placeholder' => 'Notes']) }}
            @if ($errors->has('notes'))
                <div class="error">{{ $errors->first('notes') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Total") }}
            {{ Form::text("total", null, [ 'id' => 'total', 'class' => 'form-control', 'placeholder' => 'Total']) }}
            @if ($errors->has('total'))
                <div class="error">{{ $errors->first('total') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
