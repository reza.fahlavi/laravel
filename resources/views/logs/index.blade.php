@extends('layouts.app')

@section('title','Maintanance Log')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Maintenance Log</a></li>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">List Log</h3>
          <div class="box-tools pull-right">
            <a href="{{ route('logs.create') }}" class="btn btn-sm btn-primary"> Add New Log</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="" class="text-center">No</th>
                <th width="" class="text-center">Vehicle</th>
                <th width="" class="text-center">Work Shop</th>
                <th width="" class="text-center">Date</th>
                <th width="" class="text-center">Odometer</th>
                <th width="" class="text-center">Notes</th>
                <th width="" class="text-center">Total</th>
                <th width="" class="text-center" colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @if($log->isEmpty())
                  <tr>
                    <td colspan="3" class="text-center">
                      No Data
                    </td>
                  </tr>
                @else
                  @foreach($log as $log)
                    <tr>
                     <td>{{ $loop->iteration }}</td>
                     <td>{{ $log->vehicle->name }}</td>
                     <td>{{ $log->workshop->name }}</td>
                     <td>{{ $log->date }}</td>
                     <td>{{ $log->odometer }}</td>
                     <td>{{ $log->notes }}</td>
                     <td>{{ $log->total }}</td>
                     <td class="text-center">
                     <form method="POST" action="{{ route('logs.destroy', $log->id) }}">
                      <input type="hidden" name="_method" value="delete">
                      @csrf
                      <a href="{{ route('logs.edit', $log->id) }}" class="btn btn-warning btn-sm">Edit</a>
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')">Delete</button>  
                     </form>
                     </td>
                     
                    </tr>
                  @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>  
  </div>  
@endsection
