@extends('layouts.app')

@section('title','Vehicle')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">Simple</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Vehicle</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['logs.store'], 'method' => 'POST', 'files'=> true]) !!}
        {{ Form::hidden('user_id', Auth::User()->id) }}
          <div class="form-group">
          {{ Form::label("Vehicle") }}
          <select name="vehicle_id" class="form-control" id="">
            <option value="" selected>Choose Vehicle</option>
            @foreach($vehicle as $key => $value)
              <option value="{{ $key }}" {{ $log->vehicle_id == $key ? 'selected' : null }}>{{ $value }}</option>
            @endforeach            
          </select>
            @error('vehicle_id')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
          {{ Form::label("Work Shop") }}
          <select name="workshop_id" class="form-control" id="">
            <option value="" selected>Choose Work Shop</option>
            @foreach($workshop as $key => $value)
              <option value="{{ $key }}" {{ $log->workshop_id == $key ? 'selected' : null }}>{{ $value }}</option>
            @endforeach            
          </select>
            @error('workshop_id')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
          {{ Form::label("Date") }}
            {{ Form::date("date", $log->date, [ 'id' => 'date', 'class' => 'form-control', 'placeholder' => 'Date']) }}
            @if ($errors->has('date'))
                <div class="error">{{ $errors->first('date') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Total") }}
            {{ Form::text("odometer", $log->odometer, [ 'id' => 'odometer', 'class' => 'form-control', 'placeholder' => 'Odometer']) }}
            @if ($errors->has('odometer'))
                <div class="error">{{ $errors->first('odometer') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Notes") }}
            {{ Form::textarea("notes", $log->notes, [ 'id' => 'notes', 'class' => 'form-control', 'placeholder' => 'Notes']) }}
            @if ($errors->has('notes'))
                <div class="error">{{ $errors->first('notes') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Total") }}
            {{ Form::text("total", $log->total, [ 'id' => 'total', 'class' => 'form-control', 'placeholder' => 'Total']) }}
            @if ($errors->has('total'))
                <div class="error">{{ $errors->first('total') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
