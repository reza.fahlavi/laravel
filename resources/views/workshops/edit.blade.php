@extends('layouts.app')

@section('title','Workshop')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('workshops.index')}}">Tables</a></li>
    <li class="active">New Workshop</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Quick Example</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['workshops.update', $workshops->id], 'method' => 'PUT']) !!}
          <div class="form-group">
          {{ Form::label("Name") }}
            {{ Form::text("name", $workshops->name, [ 'id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name']) }}
            @if ($errors->has('name'))
                <div class="error">{{ $errors->first('name') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Address") }}
            {{ Form::text("address", $workshops->address, [ 'id' => 'address', 'class' => 'form-control', 'placeholder' => 'Address']) }}
            @if ($errors->has('address'))
                <div class="error">{{ $errors->first('address') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Phone") }}
            {{ Form::text("phone", $workshops->phone, [ 'id' => 'phone', 'class' => 'form-control', 'placeholder' => 'Phone']) }}
            @if ($errors->has('phone'))
                <div class="error">{{ $errors->first('phone') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
