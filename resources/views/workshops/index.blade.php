@extends('layouts.app')

@section('title','Workshop')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Workshop</a></li>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-10">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">List Workshop</h3>
          <div class="box-tools pull-right">
            <a href="{{ route('workshops.create') }}" class="btn btn-sm btn-primary"> Add Workshop</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Name</th>
                <th class="text-center">Address</th>
                <th class="text-center">phone</th>
                <th class="text-center" colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
                @if($workshops->isEmpty())
                    <tr>
                        <td colspan="8" class="text-center">
                        No Data
                        </td>
                    </tr>
                @else
                    @foreach($workshops as $workshop)
                        <tr>
                           <td class="text-center">{{ $loop->iteration }}</td>
                           <td class="text-center">{{ $workshop->name }}</td>
                           <td class="text-center">{{ $workshop->address }}</td>
                           <td class="text-center">{{ $workshop->phone }}</td>
                           <td class="text-center">
                              <form method="POST" action="{{ route('workshops.destroy', $workshop->id) }}">
                                <input type="hidden" name="_method" value="delete">
                                @csrf
                                <a href="{{ route('workshops.edit', $workshop->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                  <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')">Delete</button>  
                              </form>
                           </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>  
  </div>  
@endsection
