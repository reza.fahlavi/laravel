@extends('layouts.app')

@section('title','Maintenance Log')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">New Maintenance
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Maintenance Log</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['workitems.update', $workitems->id], 'method' => 'PUT']) !!}
          <div class="form-group">
          {{ Form::label("Name") }}
            {{ Form::text("name", $workitems->name, [ 'id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name']) }}
            @if ($errors->has('name'))
                <div class="error">{{ $errors->first('name') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Next Distance") }}
            {{ Form::text("next_distance", $workitems->next_distance, [ 'id' => 'next_distance', 'class' => 'form-control', 'placeholder' => 'Next Distance']) }}
            @if ($errors->has('next_distance'))
                <div class="error">{{ $errors->first('next_distance') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Next Month") }}
            {{ Form::text("next_month", $workitems->next_month, [ 'id' => 'next_month', 'class' => 'form-control', 'placeholder' => 'Next Month']) }}
            @if ($errors->has('next_month'))
                <div class="error">{{ $errors->first('next_month') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Description") }}
            {{ Form::textarea ("description", $workitems->description, [ 'id' => 'description', 'class' => 'form-control', 'placeholder' => 'Description']) }}
            @if ($errors->has('description'))
                <div class="error">{{ $errors->first('description') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
