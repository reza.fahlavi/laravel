@extends('layouts.app')

@section('title','Work Item')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">New Work Item</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Work Item</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['workitems.store'], 'method' => 'POST', 'files'=> true]) !!}
            {{ Form::hidden('user_id', Auth::User()->id) }}
          <div class="form-group">
            {{ Form::label("Name") }}
            {{ Form::text("name", null, [ 'id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name']) }}
            @if ($errors->has('name'))
                <div class="error">{{ $errors->first('name') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Next Distance") }}
            {{ Form::text("distance", null, [ 'id' => 'distance', 'class' => 'form-control', 'placeholder' => 'Next Distance']) }}
            @if ($errors->has('distance'))
                <div class="error">{{ $errors->first('distance') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Next Month") }}
            {{ Form::text("month", null, [ 'id' => 'month', 'class' => 'form-control', 'placeholder' => 'Next Month']) }}
            @if ($errors->has('month'))
                <div class="error">{{ $errors->first('month') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Description") }}
            {{ Form::textarea("description", null, [ 'id' => 'description', 'class' => 'form-control', 'placeholder' => 'Description']) }}
            @if ($errors->has('description'))
                <div class="error">{{ $errors->first('description') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
