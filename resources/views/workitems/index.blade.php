@extends('layouts.app')

@section('title','Work Item')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"></a>Work Item</li>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">List Work Item</h3>
          <div class="box-tools pull-right">
            <a href="{{ route('workitems.create') }}" class="btn btn-sm btn-primary"> Add New Work Item</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Name</th>
                <th class="text-center">Next Distance</th>
                <th class="text-center">Next Month</th>
                <th class="text-center">Description</th>
                <th class="text-center" colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
                @if($workitems->isEmpty())
                    <tr>
                        <td colspan="8" class="text-center">
                        No Data
                        </td>
                    </tr>
                @else
                    @foreach($workitems as $work)
                        <tr>
                           <td class="text-center">{{ $loop->iteration }}</td>
                           <td class="text-center">{{ $work->name }}</td>
                           <td class="text-center">{{ $work->next_distance}}</td>
                           <td class="text-center">{{ $work->next_month }}</td>
                           <td class="text-center">{{ $work->description }}</td>
                           <td class="text-center">
                              <form method="POST" action="{{ route('workitems.destroy', $work->id) }}">
                                <input type="hidden" name="_method" value="delete">
                                @csrf
                                <a href="{{ route('workitems.edit', $work->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                  <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')">Delete</button>  
                              </form>
                           </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>  
  </div>  
@endsection
