@extends('layouts.app')

@section('title','Schedule')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">New Schedule</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Schedule</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['schedules.update', $schedule->id], 'method' => 'PUT', 'files'=> true]) !!}
          <div class="form-group">
            {{ Form::label("vehicle") }}
            <select name="vehicle_id" class="form-control" id="">
              <option value="" selected>Choose vehicle</option>
              @foreach($vehicles as $key => $value)
                <option value="{{ $key }}" {{ $schedule->vehicle_id == $key ? 'selected' : null }}>{{ $value }}</option>
              @endforeach            
            </select>
              @error('vehicle_id')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
          </div>
          <div class="form-group">
            {{ Form::label("Work Item") }}
            <select name="workitem_id" class="form-control" id="">
              <option value="" selected>Choose Work Item</option>
              @foreach($works as $key => $value)
                <option value="{{ $key }}" {{ $schedule->workitem_id == $key ? 'selected' : null }}>{{ $value }}</option>
              @endforeach            
            </select>
              @error('workitem_id')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
          </div>
          <div class="form-group">
          {{ Form::label("Status :") }}<br>
            {{Form::radio("status", '0', $schedule->status == 0 ? 'true' : null) }}Not Yet <br>
            {{Form::radio("status", '1', $schedule->status == 1 ? 'true' : null) }}Done <br>
            {{Form::radio("status", '2', $schedule->status == 2 ? 'true' : null) }}Late
            @if ($errors->has('status'))
                <div class="error">{{ $errors->first('status') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Title") }}
            {{ Form::text("title", $schedule->title, [ 'id' => 'title', 'class' => 'form-control', 'placeholder' => 'Title']) }}
            @if ($errors->has('title'))
                <div class="error">{{ $errors->first('title') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Odometer") }}
            {{ Form::text("odometer", $schedule->odometer, [ 'id' => 'odometer', 'class' => 'form-control', 'placeholder' => 'Odometer']) }}
            @if ($errors->has('odometer'))
                <div class="error">{{ $errors->first('odometer') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Date") }}
            {{ Form::date("date", $schedule->date, [ 'id' => 'date', 'class' => 'form-control', 'placeholder' => 'Date']) }}
            @if ($errors->has('date'))
                <div class="error">{{ $errors->first('date') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Description") }}
            {{ Form::textarea("description", $schedule->description, [ 'id' => 'description', 'class' => 'form-control', 'placeholder' => 'Description']) }}
            @if ($errors->has('description'))
                <div class="error">{{ $errors->first('description') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
