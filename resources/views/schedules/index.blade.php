@extends('layouts.app')

@section('title','Schedule')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"></a>Schedule</li>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">List Schedule</h3>
          <div class="box-tools pull-right">
            <a href="{{ route('schedules.create') }}" class="btn btn-sm btn-primary"> Add New Schedule</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Vehicle</th>
                <th class="text-center">Work Item</th>
                <th class="text-center">Title</th>
                <th class="text-center">Status</th>
                <th class="text-center">Date</th>
                <th class="text-center">Odometer</th>
                <th class="text-center">Description</th>
                <th class="text-center" colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
                @if($schedules->isEmpty())
                    <tr>
                        <td colspan="8" class="text-center">
                        No Data
                        </td>
                    </tr>
                @else
                    @foreach($schedules as $schedule)
                        <tr>
                           <td class="text-center">{{ $loop->iteration }}</td>
                           <td class="text-center">{{ $schedule->vehicle->name }}</td>
                           <td class="text-center">{{ $schedule->workitem->name }}</td>
                           <td class="text-center">{{ $schedule->title }}</td>
                           <td class="text-center">{{ $schedule->status}}</td>
                           <td class="text-center">{{ $schedule->date }}</td>
                           <td class="text-center">{{ $schedule->odometer }}</td>
                           <td class="text-center">{{ $schedule->description }}</td>
                           <td class="text-center">
                           <form method="POST" action="{{ route('schedules.destroy', $schedule->id) }}">
                              <input type="hidden" name="_method" value="delete">
                              @csrf
                              <a href="{{ route('schedules.edit', $schedule->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')">Delete</button>  
                            </form>
                           </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>  
  </div>  
@endsection
