@extends('layouts.app')

@section('title','Schedule')

@section('breadcrumb')  
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Tables</a></li>
    <li class="active">New Schedule</li>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">New Schedule</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        {!! Form::open(['route' => ['schedules.store'], 'method' => 'POST', 'files'=> true]) !!}
          <div class="form-group">
          {{ Form::label("Vehicle") }}
          {{ Form::select("vehicle_id", $vehicles, null , ["class" => "form-control"]) }}
            @if ($errors->has('vehicle_id'))
                <div class="error">{{ $errors->first('vehicle_id') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Work Item") }}
          {{ Form::select("workitem_id", $works, null , ["class" => "form-control"]) }}
            @if ($errors->has('workitem_id'))
                <div class="error">{{ $errors->first('workitem_id') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Status :") }}<br>
            {{Form::radio("status", '0') }}Not Yet <br>
            {{Form::radio("status", '1',true) }}Done <br>
            {{Form::radio("status", '2') }}Late
            @if ($errors->has('status'))
                <div class="error">{{ $errors->first('status') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Title") }}
            {{ Form::text("title", null, [ 'id' => 'title', 'class' => 'form-control', 'placeholder' => 'Title']) }}
            @if ($errors->has('title'))
                <div class="error">{{ $errors->first('title') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Odometer") }}
            {{ Form::text("odometer", null, [ 'id' => 'odometer', 'class' => 'form-control', 'placeholder' => 'Odometer']) }}
            @if ($errors->has('odometer'))
                <div class="error">{{ $errors->first('odometer') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Date") }}
            {{ Form::date("date", null, [ 'id' => 'date', 'class' => 'form-control', 'placeholder' => 'Date']) }}
            @if ($errors->has('date'))
                <div class="error">{{ $errors->first('date') }}</div>
            @endif
          </div>
          <div class="form-group">
          {{ Form::label("Description") }}
            {{ Form::textarea("description", null, [ 'id' => 'description', 'class' => 'form-control', 'placeholder' => 'Description']) }}
            @if ($errors->has('description'))
                <div class="error">{{ $errors->first('description') }}</div>
            @endif
          </div>
          <div class="box-footer">
            {{Form::submit('Submit', ['class'=> 'btn btn-primary']),}} 
          </div>
        {!! Form::close() !!}
        
      </div>
    </div>
  </div>
</div> 
@endsection
