<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintananceLogDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintanance_log_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('workitem_id');
            $table->unsignedBigInteger('maintenance_log_id');
            $table->decimal('amount', 12,2);
            $table->timestamps();
            $table->foreign('workitem_id')->references('id')->on('work_items');
            $table->foreign('maintenance_log_id')->references('id')->on('maintanance_logs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintanance_log_details');
    }
}
