<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect route('/home');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::resource('brands', 'BrandController');

Route::resource('vehicles', 'VehicleController');
route::resource('vehicles.services', 'ServiceController');
route::resource('vehicles.schedules', 'MaintenanceScheduleController');

route::resource('workitems', 'WorkitemController');

route::resource('workshops', 'WorkshopController');


route::resource('logs', 'MaintananceLogController');
route::resource('logdetails', 'MaintananceLogDetailController');
route::resource('schedules', 'MaintenanceScheduleController');

