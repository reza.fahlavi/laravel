<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintananceLogDetail extends Model
{
    protected $guarded = [];

    protected function MaintananceLog()
    {
        return $this->belongsTo('App\MaintananceLog', 'maintanancelogs_id');
    }

    protected function WorkItem()
    {
        return $this->belongsTo('App\WorkItem', 'workitem_id');
    }
    
}
