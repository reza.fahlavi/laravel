<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintananceSchedule extends Model
{
    protected $guarded =[];

    protected function Vehicle()
    {
        return $this->belongsTo('App\Vehicle', 'vehicle_id');
    }

    protected function WorkItem()
    {
        return $this->belongsTo('App\WorkItem', 'workitem_id');
    }
}
