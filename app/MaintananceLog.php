<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintananceLog extends Model
{
    protected $guarded = [];

    protected function MaintananceLogDetail()
    {
        return $this->hasMany('App\MaintananceLogDetail', 'maintanancelogdetails_id');
    }
    protected function Vehicle()
    {
        return $this->belongsTo('App\Vehicle', 'vehicle_id');
    }
    protected function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    protected function workshop()
    {
        return $this->belongsTo('App\Workshop', 'workshop_id');
    }
}
