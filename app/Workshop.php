<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    protected $guarded = [];

    protected function MaintananceLog()
    {
        return $this->hasMany('App\MaintananceLog', 'maintanancelog_id');
    }
}
