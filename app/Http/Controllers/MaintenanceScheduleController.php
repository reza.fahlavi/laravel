<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaintananceSchedule;
use App\Vehicle;
use App\WorkItem;
use App\VehicleInspection;

class MaintenanceScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = MaintananceSchedule::all();
        return view('schedules.index', ['schedules'=>$schedules]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Vehicle $vehicle)
    {
        $vehicles = Vehicle::pluck('name', 'id')->prepend('Choose The Vehicle','');
        $works = WorkItem::pluck('name', 'id')->prepend('Choose Work Item','');
        return view('schedules.new', ['vehicle'=>$vehicle, 'vehicles'=>$vehicles, 'works'=>$works]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Vehicle $vehicle)
    {
        $this->validate($request,[
            'vehicle_id' => 'required',
            'workitem_id' => 'required',
            'title' => 'required',
            'odometer' => 'required',
            'status' => 'required',
            'date' => 'required',
            'description' => 'required'
        ]);
        // dd($vehicle);
        $schedules = new MaintananceSchedule;
        $schedules->vehicle_id = $request->vehicle_id;
        $schedules->workitem_id = $request->workitem_id;
        $schedules->title = $request->title;
        $schedules->odometer = $request->odometer;
        $schedules->status = $request->status;
        $schedules->date = $request->date;
        $schedules->description = $request->description;
        $schedules->save();
 
        return redirect()->route('vehicles.show', ['vehicle'=>$vehicle]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle ,$schedule)
    {
        // dd($vehicle);
        $schedule = MaintananceSchedule::findorfail($schedule);
        $vehicles = Vehicle::pluck('name', 'id'); 
        $works = WorkItem::pluck('name', 'id');
        // dd($works);
        return view('vehicles.schedules.edit', ['vehicle'=>$vehicle, 'schedule'=>$schedule, 'vehicles'=>$vehicles, 'works'=>$works]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $vehicle, MaintananceSchedule $schedule)
    {
        // dd($vehicle);
        $schedule->vehicle_id = $request->vehicle_id;
        // dd($schedule);
        $schedule->workitem_id = $request->workitem_id;
        $schedule->title = $request->title;
        $schedule->odometer = $request->odometer;
        $schedule->status = $request->status;
        $schedule->date = $request->date;
        $schedule->description = $request->description;
        // dd($schedule);
        $schedule->save();
        return redirect()->route('vehicles.show', ['vehicle'=>$vehicle]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle, MaintananceSchedule $schedule)
    {
        $schedule->delete();
        return redirect()->route('vehicles.show', ['vehicle'=>$vehicle->id]);
    }
}
