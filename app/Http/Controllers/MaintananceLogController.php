<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaintananceLog;
use App\Vehicle;
use App\Workshop;

class MaintananceLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maintenancelogs = MaintananceLog::all();
        return view('logs.index', ['log'=>$maintenancelogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd('test');
        $vehicle = Vehicle::pluck('name', 'id');
        $workshop = WorkShop::pluck('name', 'id');
        // return view('logs.index', ['logs'=>$maintenancelogs]);
        return view('logs.new', ['vehicle'=>$vehicle, 'workshop'=>$workshop]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'vehicle_id' => 'required',
            'workshop_id' => 'required',
            'date' => 'required',
            'total' => 'required',
            'odometer' => 'required',
            'notes' => 'required'
        ]);
        $log = new MaintananceLog;
        $log->user_id = $request->user_id;
        $log->vehicle_id = $request->vehicle_id;
        $log->workshop_id = $request->workshop_id;
        $log->date = $request->date;
        $log->total = $request->total;
        $log->odometer = $request->odometer;
        $log->notes = $request->notes;
        $log->save();
        return redirect()->route('logs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MaintananceLog $log)
    {
        // dd($log);
        $vehicle = Vehicle::pluck('name', 'id')->prepend('Choose The Vehicle','');
        $workshop = WorkShop::pluck('name', 'id')->prepend('Choose Work Shop','');
        return view('logs.edit', ['log'=>$log, 'vehicle'=>$vehicle, 'workshop'=>$workshop]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,MaintananceLog $log)
    {
        $log->user_id = $request->user_id;
        $log->vehicle_id = $request->vehicle_id;
        $log->workshop_id = $request->workshop_id;
        $log->date = $request->date;
        $log->total = $request->total;
        $log->odometer = $request->odometer;
        $log->notes = $request->notes;
        $log->save();
        return redirect()->route('logs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaintananceLog $log)
    {
        $log->delete();
        return redirect()->route('logs.index');
    }
}
