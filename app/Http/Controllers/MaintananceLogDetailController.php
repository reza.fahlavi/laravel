<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaintananceLogDetail;
use App\MaintananceLog;
use App\WorkItem;

class MaintananceLogDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = MaintananceLogDetail::all();
        return view('logdetails.index', ['details'=>$details]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $works = WorkItem::pluck('name', 'id')->prepend('Choose The Work Item','');
        $logs = MaintananceLog::pluck('date', 'id')->prepend('Choose Log','');
        // dd($logs);
        return view('logdetails.new', ['works'=>$works, 'logs'=>$logs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'workitem_id' => 'required',
            'maintenancelog_id' => 'required',
            'amount' => 'required',
            ]);        
        $details = new MaintananceLogDetail;
        $details->workitem_id = $request->workitem_id;        
        $details->maintenance_log_id = $request->maintenancelog_id;
        $details->amount = $request->amount;
        // dd($details);
        $details->save();
        return redirect()->route('logdetails.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($detail)
    {
        $detail = MaintananceLogDetail::findorfail($detail);
        $works = WorkItem::pluck('name', 'id')->prepend('Choose The Work Item','');
        $logs = MaintananceLog::pluck('date', 'id')->prepend('Choose Log','');
        return view('logdetails.edit', ['work'=>$works, 'log'=>$logs, 'detail'=>$detail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $detail)
    {
        $detail = MaintananceLogDetail::findorfail($detail);
        $detail->workitem_id = $request->workitem_id;        
        $detail->maintenance_log_id = $request->maintenancelog_id;
        $detail->amount = $request->amount;
        // dd($detail);
        $detail->save();
        return redirect()->route('logdetails.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($detail)
    {
        $logs = MaintananceLogDetail::findorfail($detail);
        $logs->delete();
        return redirect()->route('logdetails.index');
    }
}
