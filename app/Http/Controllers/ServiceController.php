<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use App\Brand;
use App\User;
use App\VehicleInspection;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $vehicleInspections = VehicleInspection::all();
    //     $vehicles = Vehicle::all();
    //     $user = User::all();
    //     return view('services.index', ['vehicles'=>$vehicles, 'vehicle_inspections'=>$vehicleInspections]);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Vehicle $vehicle)
    {
        return view('vehicles.services.new')->with('vehicle', $vehicle);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function cari(Request $request)
    {
        //
    }

    public function store(Vehicle $vehicle, Request $request)
    {
        $this->validate($request,[
            'date'=> 'required',
            'odometer' => 'required',
            'description'=>'required'
        ]);
        // dd($request);
        $vehicle_inspections = new VehicleInspection;
        $vehicle_inspections->user_id = $request->user_id;
        $vehicle_inspections->vehicle_id = $request->vehicle_id;
        $vehicle_inspections->date = $request->date;
        $vehicle_inspections->odometer = $request->odometer;
        $vehicle_inspections->description = $request->description;
        $vehicle_inspections->save();
        return redirect()->route('vehicles.show', ['vehicle'=>$vehicle]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($vehicle ,$inspect)
    {
        $inspect = VehicleInspection::findorfail($inspect);
        $vehicle = Vehicle::findorfail($vehicle);
        // dd($inspect);
        return view('vehicles.services.edit', ['vehicle'=>$vehicle ,'inspect'=>$inspect]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $vehicle , $inspect)
    {
        $inspections = VehicleInspection::findorfail($inspect);
        // dd($inspections);
        $inspections->user_id = $request->user_id;
        $inspections->vehicle_id = $request->vehicle_id;
        $inspections->date = $request->date;
        $inspections->odometer = $request->odometer;
        $inspections->description = $request->description;
        $inspections->save();
        return redirect()->route('vehicles.show', ['vehicle'=>$vehicle]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle , VehicleInspection $service)
    {
        $service->delete();
        return redirect()->route('vehicles.show', ['vehicle'=>$vehicle->id]);
    }
}
