<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\workshop;

class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workshops = Workshop::all();
        return view('workshops.index', ['workshops'=>$workshops]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('workshops.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);
        $workshop = new Workshop;
        $workshop->name = $request->name;
        $workshop->address = $request->address;
        $workshop->phone = $request->phone;
        $workshop->save();
        return redirect()->route('workshops.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($workshop)
    {
        // dd($workshop);
        $workshop = Workshop::findorfail($workshop);
        return view('workshops.edit', ['workshops'=> $workshop]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $workshops)
    {
        // dd($workshops);
        $workshop=Workshop::findorfail($workshops);
        $workshop->name = $request->name;
        $workshop->address = $request->address;
        $workshop->phone = $request->phone;
        $workshop->save();
        return redirect()->route('workshops.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($workshops)
    {
        $workshop= Workshop::findorfail($workshops);
        $workshop->delete();
        return redirect()->route('workshops.index');
    }
}
