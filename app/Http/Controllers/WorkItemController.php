<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workitem;

class WorkItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workitems = Workitem::all();
        return view('workitems.index', ['workitems'=>$workitems]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('workitems.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'distance' => 'required',
            'month' => 'required'
        ]);
        $workitems = new WorkItem;
        $workitems->name = $request->name;
        $workitems->description = $request->description;
        $workitems->next_distance = $request->distance;
        $workitems->next_month = $request->month;
        $workitems->save();
        // Brand::create([
        //     'name' => $request->name,
        //     'code' => $request->code,
        // ]);
 
        return redirect()->route('workitems.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($work)
    {
        $work = WorkItem::findorfail($work);
        return view('workitems.edit', ['workitems'=>$work]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $workitems)
    {
        $work = WorkItem::find($workitems);   
        $work->name = $request->name;
        $work->description = $request->description;
        $work->next_distance = $request->next_distance;
        $work->next_month = $request->next_month;
        $work->save();
        return redirect()->route('workitems.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($work)
    {
        $work= WorkItem::findorfail($work);
        $work->delete();
        return redirect()->route('workitems.index');
    }
}
