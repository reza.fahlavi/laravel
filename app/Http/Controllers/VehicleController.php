<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use Auth;
use App\Brand;
use App\VehicleInspection;
use App\MaintananceSchedule;
use Storage;
use File;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vehicles = Vehicle::query();
        $filter = [];
        if (!empty($request->filter)) {
            $filter = $request->filter;
            // dd($request);
            foreach ($filter as $key => $value) {
                // dd($filter);
                $vehicles->where($key, 'like' , '%'.$value.'%');
            }
        }
        $vehicles = $vehicles->orderBy('created_at', 'desc')->paginate(5);
        // dd($vehicles);
        return view('vehicles.index', ['vehicles'=> $vehicles, 'filter' => $filter]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::pluck('name','id')->prepend('Choose Brand','');
        // dd($brands);
        return view('vehicles.new', ['brands' => $brands]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'identifier' => 'required',
            'brand_id' => 'required',
            'name' => 'required',
            'color' => 'required',
            'type' => 'required',
            'year' => 'required',
            'odometer' => 'required',
            'picture'=> 'required'
        ]);
        $vehicle = new Vehicle;
        $vehicle->identifier = $request->identifier;
        $vehicle->user_id = $request->user_id;
        $vehicle->brand_id = $request->brand_id;
        $vehicle->name = $request->name;
        $vehicle->color = $request->color;
        $vehicle->type = $request->type;
        $vehicle->year = $request->year;
        $vehicle->odometer = $request->odometer;
        $vehicle->picture = $request->picture;
        $vehicle->active = $request->status;
        // menyimpan data file yang diupload ke variabel $file
        if($request->hasFile('picture')) {
            $file = $request->file('picture');
            $filename =  time() . '_' .$file->getClientOriginalName();
            $full_path = 'vehicle/' . $filename;
            $a = Storage::disk('images')->put($full_path, File::get($request->file('picture')));
            if($a){            
                $vehicle->picture = $full_path;          
            }
        }
        $vehicle->save();
        return redirect()->route('vehicles.show', $vehicle->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        // dd($file);
        $inspect = VehicleInspection::where('vehicle_id', '=', $vehicle->id)->get();
        $schedule = MaintananceSchedule::where('vehicle_id', '=', $vehicle->id)->get();
        // dd($schedule);
        return view('vehicles.details', 
                    ['vehicle'=> $vehicle, 'inspect'=>$inspect, 'schedule'=>$schedule]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(vehicle $vehicle)
    {
        $brands = Brand::pluck('name','id');
        // dd($vehicles);
        return view('vehicles.edit', ['vehicle' => $vehicle, 'brands'=>$brands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vehicle $vehicle)
    {
        // dd($request->all());
        // $vehicle = Vehicle::findorfail($vehicles);
        $vehicle->identifier = $request->identifier;
        $vehicle->brand_id = $request->brand_id;
        $vehicle->name = $request->name;
        $vehicle->color = $request->color;
        $vehicle->type = $request->type;
        $vehicle->year = $request->year;
        $vehicle->odometer = $request->odometer;
        $vehicle->user_id = $request->user_id;
        $vehicle->active = $request->status;
        if($request->hasFile('picture')) {
            $oldfilename = $vehicle->picture;
            $oldfileexists = Storage::disk('images')->exists( $oldfilename );

            //Delete old avatar
            if($oldfilename != 'user/profile.jpg' and $oldfileexists){
                Storage::disk('images')->delete( $oldfilename );
            }

            $file = $request->file('picture');
            $filename =  time() . '_' .$file->getClientOriginalName();
            $full_path = 'vehicle/' . $filename;
            $a = Storage::disk('images')->put($full_path, File::get($request->file('picture')));
            
            if($a){            
                $vehicle->picture = $full_path;            
            }
        }
        $vehicle->save();
        return redirect()->route('vehicles.show', [$vehicle->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(vehicle $vehicle)
    {
        $vehicle->delete();
        return redirect()->route('vehicles.index');
    }
}
