<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleInspection extends Model
{
    protected $guarded = [];

    protected function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    protected function Vehicle()
    {
        return $this->belongsTo('App\Vehicle', 'vehicle_id');
    }
}
