<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkItem extends Model
{
    protected $guarded = [];

    protected function MaintananceSchedule()
    {
        return $this->hasMany('App\MaintananceSchedule', 'maintananceschedule_id');
    }

    protected function MaintananceLogDetail()
    {
        return $this->hasMany('App\MaintananceLogDetail', 'maintanancelogdetail_id');
    }
}
