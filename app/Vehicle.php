<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $guarded = [];

    protected function MaintananceSchedule()
    {
        return $this->hasMany('App\MaintananceSchedule', 'maintananceschedule_id');
    }
    protected function MaintananceLog()
    {
        return $this->hasMany('App\MaintananceLog', 'maintanancelog_id');
    }
    protected function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    protected function VehicleInspection()
    {
        return $this->belongsTo('App\VehicleInspection', 'vehicle_id');
    }
    protected function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }
}
