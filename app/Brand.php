<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $guarded = [];
    
    Protected function Vehicles()
    {
        return $this->hasMany('App\Vehicle', 'vehicle_id');
    }
}
